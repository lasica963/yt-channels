const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const assetsDir = 'static/';

module.exports = {
  target: 'node',
  entry: {
    'js/core.js': ['./src/js/_core.js']
  },
  output: {
    path: path.resolve(__dirname, './' + assetsDir),
    filename: '[name]'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            ["@babel/preset-env", {
              loose: true,
              modules: false,
            }
            ]
          ],
        }
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin({
      filename: '[name]',
      allChunks: true
    }),
    // new webpack.optimize.UglifyJsPlugin({
    //   comments: false,
    //   ie8: false,
    //   ecma: 8,
    //   mangle: false,
    //   beautify: true,
    //   compress: false,
    //   cache: false,
    //   parallel: 8
    // })
  ]
};