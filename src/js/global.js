window.putComma = (num) => {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

window.debounce = (func, delay) => {
    let debounceTimer;
    return function () {
        const context = this;
        const args = arguments;
        clearTimeout(debounceTimer);
        debounceTimer = setTimeout(() => func.apply(context, args), delay)
    }
};

const explodeDotNotation = (obj, notation) => notation.split('.').reduce((object, index) => object[index], obj);

Array.prototype.sortByPropNum = function (propName) {
    return this.sort((a, b) => explodeDotNotation(b, propName) - explodeDotNotation(a, propName));
};

Array.prototype.sortByPropString = function (propName) {
    return this.sort((a, b) => explodeDotNotation(a, propName) > explodeDotNotation(b, propName) ? 1 : -1);
};

