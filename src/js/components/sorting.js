import channels from "../utils/_channels";
import data from "../_core";

class Sorting {
  // Passing in our state object and setting it as a class property.
  constructor(state = {}) {
    this.appState = state;
  }

  // Bind an event on submit for the add user form.
  bindEvents() {

    document.getElementById("sort-title").addEventListener("click", () => {

      const state = this.appState.get();

      this.appState.update({
        ...state,
        channels: state.channels.sortByPropString('title')
      });

    });

    document.getElementById("sort-subscribers").addEventListener("click", () => {
      const state = this.appState.get();

      this.appState.update({
        ...state,
        channels: state.channels.sortByPropNum('statistics.subscriberCount')

      });
    });

    document.getElementById("sort-videos").addEventListener("click", () => {
      const state = this.appState.get();

      this.appState.update({
        ...state,
        channels: state.channels.sortByPropNum('statistics.videoCount')

      });
    });

    document.getElementById("sort-views").addEventListener("click", () => {
      const state = this.appState.get();

      this.appState.update({
        ...state,
        channels: state.channels.sortByPropNum('statistics.viewCount')

      });
    });

    document.getElementById("sort__clear").addEventListener("click", () => {

        this.appState.update({
        ...channels,
        channels: channels
      });
    });

  }
}

export default Sorting;
