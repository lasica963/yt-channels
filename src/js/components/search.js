export class search {
    constructor() {
        if (!this._setVars()) return;
        this._setEvents();
    }

    _setVars() {
        let _this = this;

        _this.searchInput = document.querySelector('.filter__input');
        if (!_this.searchInput) return false;

        return true;
    }

    _setEvents() {
        this.search();
    }

    search() {
        let boxes = document.querySelectorAll('.wrapper__main__item');

        this.searchInput.addEventListener('keyup', debounce(() => {
            boxes.forEach(item => {
                if (item.getAttribute('data-title').toUpperCase().indexOf(this.searchInput.value.toUpperCase()) > -1) {

                    item.style.display = 'block';

                } else {
                    item.style.display = 'none';
                }
            });
        }, 200));

    }

}
