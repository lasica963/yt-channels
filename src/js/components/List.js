import Observer from "../lib/Observer";

class List extends Observer {
    createMarkup(state) {
        return `
        <div class="wrapper__main">
            ${state.channels.map(chanel => `<div class="wrapper__main__item" data-title="${chanel.title}">
                  <div>
                
                    <a href="${chanel.customUrl}" target="_blank">
                      <img src="${chanel.thumbnails.medium.url}" alt="${chanel.title}" class="item__img">
                    </a>
                
                    <h6 class="item__title" data-prop="title">${chanel.title}</h6>
                
                    <div class="item__info">
                      <div>
                        <span>SUBSCRIBERS</span>
                        <span class="item__count">${putComma(chanel.statistics.subscriberCount)}</span>
                      </div>
                      <div>
                        <span>VIDEOS</span>
                        <span class="item__count">${putComma(chanel.statistics.videoCount)}</span>
                      </div>
                      <div>
                        <span>VIEWS</span>
                        <span class="item__count">${putComma(chanel.statistics.viewCount)}</span>
                      </div>
                    </div>
                  </div>
                </div>`).join("\n")}
        </div>
    `;
    }

    render(state, selector = "app") {
        const markup = this.createMarkup(state);
        const parent = document.getElementById(selector);

        parent.innerHTML = markup;
    }

    // This method will be called by the Subject(state) whenever it updates.
    // Notice how it prompts a re-render.
    update(state) {
        this.render(state, "channel-list-container");
    }
}

export default List;
