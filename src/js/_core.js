// sayHello = function () {
//     return 'hello';
// };
module.exports = function () {
    return 'hello';
};


import {} from "./global";

import channels from "./channels";

import State from "./lib/State";
import List from "./components/List";
import Sorting from "./components/sorting";
import {search} from "./components/search";


// Instantiate classes.
const AppState = new State(); // application state
const channelsList = new List(); // Create a new channel list.
const filterForm = new Sorting(AppState); // Create a new sorting form.

// Hydrate state with initial channels.
AppState.update({channels});

// Add the observers. These objects will re-render when state changes.
AppState.addObserver(channelsList);

// On load, perform initial render..
channelsList.render(AppState.get(), "channel-list-container");

filterForm.bindEvents();

new search();

