/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__global__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__channels__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__channels___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__channels__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lib_State__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_List__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_sorting__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_search__ = __webpack_require__(11);
// sayHello = function () {
//     return 'hello';
// };
module.exports = function () {
  return 'hello';
};






 // Instantiate classes.

var AppState = new __WEBPACK_IMPORTED_MODULE_2__lib_State__["a" /* default */](); // application state

var channelsList = new __WEBPACK_IMPORTED_MODULE_3__components_List__["a" /* default */](); // Create a new channel list.

var filterForm = new __WEBPACK_IMPORTED_MODULE_4__components_sorting__["a" /* default */](AppState); // Create a new sorting form.
// Hydrate state with initial channels.

AppState.update({
  channels: __WEBPACK_IMPORTED_MODULE_1__channels___default.a
}); // Add the observers. These objects will re-render when state changes.

AppState.addObserver(channelsList); // On load, perform initial render..

channelsList.render(AppState.get(), "channel-list-container");
filterForm.bindEvents();
new __WEBPACK_IMPORTED_MODULE_5__components_search__["a" /* search */]();
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)(module)))

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(0);


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = function(originalModule) {
	if(!originalModule.webpackPolyfill) {
		var module = Object.create(originalModule);
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		Object.defineProperty(module, "exports", {
			enumerable: true,
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 3 */
/***/ (function(module, exports) {

window.putComma = function (num) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

window.debounce = function (func, delay) {
  var debounceTimer;
  return function () {
    var context = this;
    var args = arguments;
    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(function () {
      return func.apply(context, args);
    }, delay);
  };
};

var explodeDotNotation = function explodeDotNotation(obj, notation) {
  return notation.split('.').reduce(function (object, index) {
    return object[index];
  }, obj);
};

Array.prototype.sortByPropNum = function (propName) {
  return this.sort(function (a, b) {
    return explodeDotNotation(b, propName) - explodeDotNotation(a, propName);
  });
};

Array.prototype.sortByPropString = function (propName) {
  return this.sort(function (a, b) {
    return explodeDotNotation(a, propName) > explodeDotNotation(b, propName) ? 1 : -1;
  });
};

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = [{"title":"Fun Fun Function","description":"I’m Mattias Petter Johansson, mpj for short. I’ve been a full-time programmer for about ten years. Among others, I've worked for Absolut Vodka, Blackberry and Spotify.  https://patreon.com/funfunfunction","customUrl":"https://youtube.com/funfunfunction","publishedAt":"2015-05-15T08:25:01.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAzjMJRUuJm10kRK7ybMLDmXtRH71_F1YapMjw=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAzjMJRUuJm10kRK7ybMLDmXtRH71_F1YapMjw=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAzjMJRUuJm10kRK7ybMLDmXtRH71_F1YapMjw=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"Fun Fun Function","description":"I’m Mattias Petter Johansson, mpj for short. I’ve been a full-time programmer for about ten years. Among others, I've worked for Absolut Vodka, Blackberry and Spotify.  https://patreon.com/funfunfunction"},"country":"SE","statistics":{"viewCount":"7261372","commentCount":"0","subscriberCount":"180691","hiddenSubscriberCount":false,"videoCount":"189"}},{"title":"Google Chrome Developers","description":"Making the web more awesome.","customUrl":"https://youtube.com/ChromeDevelopers","publishedAt":"2012-04-24T00:05:52.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAx7nOfbTWCEWQl33_t90eHhqz0cSmlRElL44g=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAx7nOfbTWCEWQl33_t90eHhqz0cSmlRElL44g=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAx7nOfbTWCEWQl33_t90eHhqz0cSmlRElL44g=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"Google Chrome Developers","description":"Making the web more awesome."},"statistics":{"viewCount":"21089248","commentCount":"0","subscriberCount":"260317","hiddenSubscriberCount":false,"videoCount":"796"}},{"title":"DevTips","description":"DevTips is a weekly show for YOU who want to be inspired 👍 and learn 🖖 about programming. Hosted by David and MPJ - two notorious bug generators 💖 and teachers 🤗. Exploring code together and learning programming along the way - yay!\n\nEverything you see and hear are the opinions and preferences of the individual who said them, and no one else's.","customUrl":"https://youtube.com/DevTipsForDesigners","publishedAt":"2013-08-06T15:31:46.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAwJXtTrxzaTOzCl6Jd69lHLnrS_l2qYJKDeUw=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAwJXtTrxzaTOzCl6Jd69lHLnrS_l2qYJKDeUw=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAwJXtTrxzaTOzCl6Jd69lHLnrS_l2qYJKDeUw=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"DevTips","description":"DevTips is a weekly show for YOU who want to be inspired 👍 and learn 🖖 about programming. Hosted by David and MPJ - two notorious bug generators 💖 and teachers 🤗. Exploring code together and learning programming along the way - yay!\n\nEverything you see and hear are the opinions and preferences of the individual who said them, and no one else's."},"country":"SE","statistics":{"viewCount":"13948443","commentCount":"0","subscriberCount":"306828","hiddenSubscriberCount":false,"videoCount":"329"}},{"title":"freeCodeCamp.org","description":"We're an open source community of busy people who learn to code and build projects for nonprofits.","customUrl":"https://youtube.com/freecodecamp","publishedAt":"2014-12-16T21:18:48.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAxCZFxULNr0S_fHjEvGVUMwJzjbwU-wVularA=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAxCZFxULNr0S_fHjEvGVUMwJzjbwU-wVularA=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAxCZFxULNr0S_fHjEvGVUMwJzjbwU-wVularA=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"freeCodeCamp.org","description":"We're an open source community of busy people who learn to code and build projects for nonprofits."},"country":"US","statistics":{"viewCount":"10578494","commentCount":"0","subscriberCount":"464998","hiddenSubscriberCount":false,"videoCount":"879"}},{"title":"meet.js","description":"meet.js videos","customUrl":"https://youtube.com/meetjs","publishedAt":"2013-10-26T08:46:30.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAwgCxBPLKDOxEY31LAmTUsslMnIaNFuMGhluw=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAwgCxBPLKDOxEY31LAmTUsslMnIaNFuMGhluw=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAwgCxBPLKDOxEY31LAmTUsslMnIaNFuMGhluw=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"meet.js","description":"meet.js videos"},"statistics":{"viewCount":"28565","commentCount":"0","subscriberCount":"504","hiddenSubscriberCount":false,"videoCount":"69"}},{"title":"JSConf","description":"The JSConf YouTube Channel.\nJSConf is a series of JavaScript conferences from around the world and here we release the conference talk videos for free as fast as we can after every event.\n\nTopics include JavaScript, HTML5, CSS, node.js, robotics, etc.","customUrl":"https://youtube.com/jsconfeu","publishedAt":"2012-10-09T10:21:46.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAxWC2To0-1xz2-X3HNC6WFdP8zPKKqqHk_8ug=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAxWC2To0-1xz2-X3HNC6WFdP8zPKKqqHk_8ug=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAxWC2To0-1xz2-X3HNC6WFdP8zPKKqqHk_8ug=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"JSConf","description":"The JSConf YouTube Channel.\nJSConf is a series of JavaScript conferences from around the world and here we release the conference talk videos for free as fast as we can after every event.\n\nTopics include JavaScript, HTML5, CSS, node.js, robotics, etc."},"statistics":{"viewCount":"6747555","commentCount":"0","subscriberCount":"106248","hiddenSubscriberCount":false,"videoCount":"1281"}},{"title":"Coding Tech","description":"LEGAL DISCLAIMER ON CONTENT PERMISSIONS:\n\nCoding Tech partners with the tech conferences around the world that provided Coding Tech with their EXPLICIT permissions to republish videos on this channel. Here's the list of Conferences that provided Coding Tech with EXPLICIT written publication permissions: PyData, MLConf, Build Stuff, Code::dive, PolyConf, AmsterdamJS, React Amsterdam, ReactiveConf, Jazoon Tech Days, SFNode, CppCon, Infinite Red, GDG Lviv, Mibuso.com, You Gotta Love Frontend, React Native EU, Node Summit, NodeConfEU, ConFoo, Pixels Camp, JavaScriptLA, The Linux Foundation, Oredev Conference, Techlahoma, fitcevents. Coding Tech also re-posts content which was originally published with the Creative Commons Attribution license (reuse allowed).\n\nFor business inquiries, please connect on LinkedIn: https://www.linkedin.com/in/yuriy-matso-4614951/","customUrl":"https://youtube.com/codingtech","publishedAt":"2015-10-17T02:48:18.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAyBKLTLdTfmsp2PPjv-h53iokGAOXzcUVixRA=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAyBKLTLdTfmsp2PPjv-h53iokGAOXzcUVixRA=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAyBKLTLdTfmsp2PPjv-h53iokGAOXzcUVixRA=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"Coding Tech","description":"LEGAL DISCLAIMER ON CONTENT PERMISSIONS:\n\nCoding Tech partners with the tech conferences around the world that provided Coding Tech with their EXPLICIT permissions to republish videos on this channel. Here's the list of Conferences that provided Coding Tech with EXPLICIT written publication permissions: PyData, MLConf, Build Stuff, Code::dive, PolyConf, AmsterdamJS, React Amsterdam, ReactiveConf, Jazoon Tech Days, SFNode, CppCon, Infinite Red, GDG Lviv, Mibuso.com, You Gotta Love Frontend, React Native EU, Node Summit, NodeConfEU, ConFoo, Pixels Camp, JavaScriptLA, The Linux Foundation, Oredev Conference, Techlahoma, fitcevents. Coding Tech also re-posts content which was originally published with the Creative Commons Attribution license (reuse allowed).\n\nFor business inquiries, please connect on LinkedIn: https://www.linkedin.com/in/yuriy-matso-4614951/"},"country":"US","statistics":{"viewCount":"13779683","commentCount":"0","subscriberCount":"362617","hiddenSubscriberCount":false,"videoCount":"539"}},{"title":"Allegro Tech","description":"At Allegro, we build and maintain some of the most distributed and scalable applications in Central Europe. This poses many challenges, starting with architecture and design, through the choice of technologies, code quality and performance tuning, and ending with deployment and devops. On our blog, our engineers share their experiences and write about some of the interesting things taking place at the company.","customUrl":"https://youtube.com/allegrotechblog","publishedAt":"2015-08-07T10:54:19.000Z","thumbnails":{"default":{"url":"https://yt3.ggpht.com/a-/AN66SAxEMnUE_PX1BtYnHAIHo0-SHO0fx340qi7XSg=s88-mo-c-c0xffffffff-rj-k-no","width":88,"height":88},"medium":{"url":"https://yt3.ggpht.com/a-/AN66SAxEMnUE_PX1BtYnHAIHo0-SHO0fx340qi7XSg=s240-mo-c-c0xffffffff-rj-k-no","width":240,"height":240},"high":{"url":"https://yt3.ggpht.com/a-/AN66SAxEMnUE_PX1BtYnHAIHo0-SHO0fx340qi7XSg=s800-mo-c-c0xffffffff-rj-k-no","width":800,"height":800}},"localized":{"title":"Allegro Tech","description":"At Allegro, we build and maintain some of the most distributed and scalable applications in Central Europe. This poses many challenges, starting with architecture and design, through the choice of technologies, code quality and performance tuning, and ending with deployment and devops. On our blog, our engineers share their experiences and write about some of the interesting things taking place at the company."},"country":"PL","statistics":{"viewCount":"38100","commentCount":"0","subscriberCount":"577","hiddenSubscriberCount":false,"videoCount":"96"}}]

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Subject__ = __webpack_require__(6);
function _inheritsLoose(subClass, superClass) { subClass.prototype = Object.create(superClass.prototype); subClass.prototype.constructor = subClass; subClass.__proto__ = superClass; }



var State =
/*#__PURE__*/
function (_Subject) {
  _inheritsLoose(State, _Subject);

  function State() {
    var _this;

    _this = _Subject.call(this) || this;
    _this.state = {};
    return _this;
  } // Update the state.
  // Calls the update method on each observer.


  var _proto = State.prototype;

  _proto.update = function update(data) {
    if (data === void 0) {
      data = {};
    }

    this.state = Object.assign(this.state, data);
    this.notify(this.state);
  } // Get the state.
  ;

  _proto.get = function get() {
    return this.state;
  };

  return State;
}(__WEBPACK_IMPORTED_MODULE_0__Subject__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (State);

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var Subject =
/*#__PURE__*/
function () {
  function Subject() {
    this.observers = [];
  } // Add an observer to this.observers.


  var _proto = Subject.prototype;

  _proto.addObserver = function addObserver(observer) {
    this.observers.push(observer);
  } // Remove an observer from this.observers.
  ;

  _proto.removeObserver = function removeObserver(observer) {
    var removeIndex = this.observers.findIndex(function (obs) {
      return observer === obs;
    });

    if (removeIndex !== -1) {
      this.observers = this.observers.slice(removeIndex, 1);
    }
  } // Loops over this.observers and calls the update method on each observer.
  // The state object will call this method everytime it is updated.
  ;

  _proto.notify = function notify(data) {
    if (this.observers.length > 0) {
      this.observers.forEach(function (observer) {
        return observer.update(data);
      });
    }
  };

  return Subject;
}();

/* harmony default export */ __webpack_exports__["a"] = (Subject);

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib_Observer__ = __webpack_require__(8);
function _inheritsLoose(subClass, superClass) { subClass.prototype = Object.create(superClass.prototype); subClass.prototype.constructor = subClass; subClass.__proto__ = superClass; }



var List =
/*#__PURE__*/
function (_Observer) {
  _inheritsLoose(List, _Observer);

  function List() {
    return _Observer.apply(this, arguments) || this;
  }

  var _proto = List.prototype;

  _proto.createMarkup = function createMarkup(state) {
    return "\n        <div class=\"wrapper__main\">\n            " + state.channels.map(function (chanel) {
      return "<div class=\"wrapper__main__item\" data-title=\"" + chanel.title + "\">\n                  <div>\n                \n                    <a href=\"" + chanel.customUrl + "\" target=\"_blank\">\n                      <img src=\"" + chanel.thumbnails.medium.url + "\" alt=\"" + chanel.title + "\" class=\"item__img\">\n                    </a>\n                \n                    <h6 class=\"item__title\" data-prop=\"title\">" + chanel.title + "</h6>\n                \n                    <div class=\"item__info\">\n                      <div>\n                        <span>SUBSCRIBERS</span>\n                        <span class=\"item__count\">" + putComma(chanel.statistics.subscriberCount) + "</span>\n                      </div>\n                      <div>\n                        <span>VIDEOS</span>\n                        <span class=\"item__count\">" + putComma(chanel.statistics.videoCount) + "</span>\n                      </div>\n                      <div>\n                        <span>VIEWS</span>\n                        <span class=\"item__count\">" + putComma(chanel.statistics.viewCount) + "</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>";
    }).join("\n") + "\n        </div>\n    ";
  };

  _proto.render = function render(state, selector) {
    if (selector === void 0) {
      selector = "app";
    }

    var markup = this.createMarkup(state);
    var parent = document.getElementById(selector);
    parent.innerHTML = markup;
  } // This method will be called by the Subject(state) whenever it updates.
  // Notice how it prompts a re-render.
  ;

  _proto.update = function update(state) {
    this.render(state, "channel-list-container");
  };

  return List;
}(__WEBPACK_IMPORTED_MODULE_0__lib_Observer__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (List);

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var Observer =
/*#__PURE__*/
function () {
  function Observer() {}

  var _proto = Observer.prototype;

  // Gets called by the Subject::notify method.
  _proto.update = function update() {};

  return Observer;
}();

/* harmony default export */ __webpack_exports__["a"] = (Observer);

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_channels__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core__ = __webpack_require__(0);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




var Sorting =
/*#__PURE__*/
function () {
  // Passing in our state object and setting it as a class property.
  function Sorting(state) {
    if (state === void 0) {
      state = {};
    }

    this.appState = state;
  } // Bind an event on submit for the add user form.


  var _proto = Sorting.prototype;

  _proto.bindEvents = function bindEvents() {
    var _this = this;

    document.getElementById("sort-title").addEventListener("click", function () {
      var state = _this.appState.get();

      _this.appState.update(_extends({}, state, {
        channels: state.channels.sortByPropString('title')
      }));
    });
    document.getElementById("sort-subscribers").addEventListener("click", function () {
      var state = _this.appState.get();

      _this.appState.update(_extends({}, state, {
        channels: state.channels.sortByPropNum('statistics.subscriberCount')
      }));
    });
    document.getElementById("sort-videos").addEventListener("click", function () {
      var state = _this.appState.get();

      _this.appState.update(_extends({}, state, {
        channels: state.channels.sortByPropNum('statistics.videoCount')
      }));
    });
    document.getElementById("sort-views").addEventListener("click", function () {
      var state = _this.appState.get();

      _this.appState.update(_extends({}, state, {
        channels: state.channels.sortByPropNum('statistics.viewCount')
      }));
    });
    document.getElementById("sort__clear").addEventListener("click", function () {
      _this.appState.update(_extends({}, __WEBPACK_IMPORTED_MODULE_0__utils_channels__["a" /* default */], {
        channels: __WEBPACK_IMPORTED_MODULE_0__utils_channels__["a" /* default */]
      }));
    });
  };

  return Sorting;
}();

/* harmony default export */ __webpack_exports__["a"] = (Sorting);

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// Initial set of channels.
/* harmony default export */ __webpack_exports__["a"] = ([{
  "title": "Fun Fun Function",
  "description": "I’m Mattias Petter Johansson, mpj for short. I’ve been a full-time programmer for about ten years. Among others, I've worked for Absolut Vodka, Blackberry and Spotify.  https://patreon.com/funfunfunction",
  "customUrl": "https://youtube.com/funfunfunction",
  "publishedAt": "2015-05-15T08:25:01.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAzjMJRUuJm10kRK7ybMLDmXtRH71_F1YapMjw=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAzjMJRUuJm10kRK7ybMLDmXtRH71_F1YapMjw=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAzjMJRUuJm10kRK7ybMLDmXtRH71_F1YapMjw=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "Fun Fun Function",
    "description": "I’m Mattias Petter Johansson, mpj for short. I’ve been a full-time programmer for about ten years. Among others, I've worked for Absolut Vodka, Blackberry and Spotify.  https://patreon.com/funfunfunction"
  },
  "country": "SE",
  "statistics": {
    "viewCount": "7261372",
    "commentCount": "0",
    "subscriberCount": "180691",
    "hiddenSubscriberCount": false,
    "videoCount": "189"
  }
}, {
  "title": "Google Chrome Developers",
  "description": "Making the web more awesome.",
  "customUrl": "https://youtube.com/ChromeDevelopers",
  "publishedAt": "2012-04-24T00:05:52.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAx7nOfbTWCEWQl33_t90eHhqz0cSmlRElL44g=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAx7nOfbTWCEWQl33_t90eHhqz0cSmlRElL44g=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAx7nOfbTWCEWQl33_t90eHhqz0cSmlRElL44g=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "Google Chrome Developers",
    "description": "Making the web more awesome."
  },
  "statistics": {
    "viewCount": "21089248",
    "commentCount": "0",
    "subscriberCount": "260317",
    "hiddenSubscriberCount": false,
    "videoCount": "796"
  }
}, {
  "title": "DevTips",
  "description": "DevTips is a weekly show for YOU who want to be inspired 👍 and learn 🖖 about programming. Hosted by David and MPJ - two notorious bug generators 💖 and teachers 🤗. Exploring code together and learning programming along the way - yay!\n\nEverything you see and hear are the opinions and preferences of the individual who said them, and no one else's.",
  "customUrl": "https://youtube.com/DevTipsForDesigners",
  "publishedAt": "2013-08-06T15:31:46.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAwJXtTrxzaTOzCl6Jd69lHLnrS_l2qYJKDeUw=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAwJXtTrxzaTOzCl6Jd69lHLnrS_l2qYJKDeUw=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAwJXtTrxzaTOzCl6Jd69lHLnrS_l2qYJKDeUw=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "DevTips",
    "description": "DevTips is a weekly show for YOU who want to be inspired 👍 and learn 🖖 about programming. Hosted by David and MPJ - two notorious bug generators 💖 and teachers 🤗. Exploring code together and learning programming along the way - yay!\n\nEverything you see and hear are the opinions and preferences of the individual who said them, and no one else's."
  },
  "country": "SE",
  "statistics": {
    "viewCount": "13948443",
    "commentCount": "0",
    "subscriberCount": "306828",
    "hiddenSubscriberCount": false,
    "videoCount": "329"
  }
}, {
  "title": "freeCodeCamp.org",
  "description": "We're an open source community of busy people who learn to code and build projects for nonprofits.",
  "customUrl": "https://youtube.com/freecodecamp",
  "publishedAt": "2014-12-16T21:18:48.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxCZFxULNr0S_fHjEvGVUMwJzjbwU-wVularA=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxCZFxULNr0S_fHjEvGVUMwJzjbwU-wVularA=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxCZFxULNr0S_fHjEvGVUMwJzjbwU-wVularA=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "freeCodeCamp.org",
    "description": "We're an open source community of busy people who learn to code and build projects for nonprofits."
  },
  "country": "US",
  "statistics": {
    "viewCount": "10578494",
    "commentCount": "0",
    "subscriberCount": "464998",
    "hiddenSubscriberCount": false,
    "videoCount": "879"
  }
}, {
  "title": "meet.js",
  "description": "meet.js videos",
  "customUrl": "https://youtube.com/meetjs",
  "publishedAt": "2013-10-26T08:46:30.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAwgCxBPLKDOxEY31LAmTUsslMnIaNFuMGhluw=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAwgCxBPLKDOxEY31LAmTUsslMnIaNFuMGhluw=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAwgCxBPLKDOxEY31LAmTUsslMnIaNFuMGhluw=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "meet.js",
    "description": "meet.js videos"
  },
  "statistics": {
    "viewCount": "28565",
    "commentCount": "0",
    "subscriberCount": "504",
    "hiddenSubscriberCount": false,
    "videoCount": "69"
  }
}, {
  "title": "JSConf",
  "description": "The JSConf YouTube Channel.\nJSConf is a series of JavaScript conferences from around the world and here we release the conference talk videos for free as fast as we can after every event.\n\nTopics include JavaScript, HTML5, CSS, node.js, robotics, etc.",
  "customUrl": "https://youtube.com/jsconfeu",
  "publishedAt": "2012-10-09T10:21:46.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxWC2To0-1xz2-X3HNC6WFdP8zPKKqqHk_8ug=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxWC2To0-1xz2-X3HNC6WFdP8zPKKqqHk_8ug=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxWC2To0-1xz2-X3HNC6WFdP8zPKKqqHk_8ug=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "JSConf",
    "description": "The JSConf YouTube Channel.\nJSConf is a series of JavaScript conferences from around the world and here we release the conference talk videos for free as fast as we can after every event.\n\nTopics include JavaScript, HTML5, CSS, node.js, robotics, etc."
  },
  "statistics": {
    "viewCount": "6747555",
    "commentCount": "0",
    "subscriberCount": "106248",
    "hiddenSubscriberCount": false,
    "videoCount": "1281"
  }
}, {
  "title": "Coding Tech",
  "description": "LEGAL DISCLAIMER ON CONTENT PERMISSIONS:\n\nCoding Tech partners with the tech conferences around the world that provided Coding Tech with their EXPLICIT permissions to republish videos on this channel. Here's the list of Conferences that provided Coding Tech with EXPLICIT written publication permissions: PyData, MLConf, Build Stuff, Code::dive, PolyConf, AmsterdamJS, React Amsterdam, ReactiveConf, Jazoon Tech Days, SFNode, CppCon, Infinite Red, GDG Lviv, Mibuso.com, You Gotta Love Frontend, React Native EU, Node Summit, NodeConfEU, ConFoo, Pixels Camp, JavaScriptLA, The Linux Foundation, Oredev Conference, Techlahoma, fitcevents. Coding Tech also re-posts content which was originally published with the Creative Commons Attribution license (reuse allowed).\n\nFor business inquiries, please connect on LinkedIn: https://www.linkedin.com/in/yuriy-matso-4614951/",
  "customUrl": "https://youtube.com/codingtech",
  "publishedAt": "2015-10-17T02:48:18.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAyBKLTLdTfmsp2PPjv-h53iokGAOXzcUVixRA=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAyBKLTLdTfmsp2PPjv-h53iokGAOXzcUVixRA=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAyBKLTLdTfmsp2PPjv-h53iokGAOXzcUVixRA=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "Coding Tech",
    "description": "LEGAL DISCLAIMER ON CONTENT PERMISSIONS:\n\nCoding Tech partners with the tech conferences around the world that provided Coding Tech with their EXPLICIT permissions to republish videos on this channel. Here's the list of Conferences that provided Coding Tech with EXPLICIT written publication permissions: PyData, MLConf, Build Stuff, Code::dive, PolyConf, AmsterdamJS, React Amsterdam, ReactiveConf, Jazoon Tech Days, SFNode, CppCon, Infinite Red, GDG Lviv, Mibuso.com, You Gotta Love Frontend, React Native EU, Node Summit, NodeConfEU, ConFoo, Pixels Camp, JavaScriptLA, The Linux Foundation, Oredev Conference, Techlahoma, fitcevents. Coding Tech also re-posts content which was originally published with the Creative Commons Attribution license (reuse allowed).\n\nFor business inquiries, please connect on LinkedIn: https://www.linkedin.com/in/yuriy-matso-4614951/"
  },
  "country": "US",
  "statistics": {
    "viewCount": "13779683",
    "commentCount": "0",
    "subscriberCount": "362617",
    "hiddenSubscriberCount": false,
    "videoCount": "539"
  }
}, {
  "title": "Allegro Tech",
  "description": "At Allegro, we build and maintain some of the most distributed and scalable applications in Central Europe. This poses many challenges, starting with architecture and design, through the choice of technologies, code quality and performance tuning, and ending with deployment and devops. On our blog, our engineers share their experiences and write about some of the interesting things taking place at the company.",
  "customUrl": "https://youtube.com/allegrotechblog",
  "publishedAt": "2015-08-07T10:54:19.000Z",
  "thumbnails": {
    "default": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxEMnUE_PX1BtYnHAIHo0-SHO0fx340qi7XSg=s88-mo-c-c0xffffffff-rj-k-no",
      "width": 88,
      "height": 88
    },
    "medium": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxEMnUE_PX1BtYnHAIHo0-SHO0fx340qi7XSg=s240-mo-c-c0xffffffff-rj-k-no",
      "width": 240,
      "height": 240
    },
    "high": {
      "url": "https://yt3.ggpht.com/a-/AN66SAxEMnUE_PX1BtYnHAIHo0-SHO0fx340qi7XSg=s800-mo-c-c0xffffffff-rj-k-no",
      "width": 800,
      "height": 800
    }
  },
  "localized": {
    "title": "Allegro Tech",
    "description": "At Allegro, we build and maintain some of the most distributed and scalable applications in Central Europe. This poses many challenges, starting with architecture and design, through the choice of technologies, code quality and performance tuning, and ending with deployment and devops. On our blog, our engineers share their experiences and write about some of the interesting things taking place at the company."
  },
  "country": "PL",
  "statistics": {
    "viewCount": "38100",
    "commentCount": "0",
    "subscriberCount": "577",
    "hiddenSubscriberCount": false,
    "videoCount": "96"
  }
}]);

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return search; });
var search =
/*#__PURE__*/
function () {
  function search() {
    if (!this._setVars()) return;

    this._setEvents();
  }

  var _proto = search.prototype;

  _proto._setVars = function _setVars() {
    var _this = this;

    _this.searchInput = document.querySelector('.filter__input');
    if (!_this.searchInput) return false;
    return true;
  };

  _proto._setEvents = function _setEvents() {
    this.search();
  };

  _proto.search = function search() {
    var _this2 = this;

    var boxes = document.querySelectorAll('.wrapper__main__item');
    this.searchInput.addEventListener('keyup', debounce(function () {
      boxes.forEach(function (item) {
        if (item.getAttribute('data-title').toUpperCase().indexOf(_this2.searchInput.value.toUpperCase()) > -1) {
          item.style.display = 'block';
        } else {
          item.style.display = 'none';
        }
      });
    }, 200));
  };

  return search;
}();

/***/ })
/******/ ]);