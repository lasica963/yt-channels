var assert = require('chai').assert;
var expect = require('chai').expect;
require('../src/js/global');

describe('sorting numbers', function () {

    it('check if objects are sorted correctly', function () {
        var o1 = { property: 1 };
        var o2 = { property: 3 };
        var o3 = { property: 2 };

        var arr = [o1, o2, o3];

        var result = arr.sortByPropNum('property');

        expect(result).to.have.deep.ordered.members([o2, o3, o1]);
    })
});

describe('sorting string', function () {

    it('check if objects are sorted correctly', function () {
        var o1 = { property: 'a' };
        var o2 = { property: 'c' };
        var o3 = { property: 'b' };

        var arr = [o1, o2, o3];

        var result = arr.sortByPropString('property');

        expect(result).to.have.deep.ordered.members([o1, o3, o2]);
    })
});

describe('putComma function', function () {
    it('check ', function () {
        var num = 10000;

        var result = window.putComma(num);

        assert.equal(result, '10,000')
    })
})
