## JS Youtube Channels 

### Reactive DOM in pure JS 

App supports reactive DOM like a React framework, you can observe state, manipulation, update and render DOM based on this as you wish. 
All functions off engines in `./lib/`

### Important functions for reactive DOM

```bash
// Update the state.
// Calls the update method on each observer.
this.appState.update({
    ...state,
    channels: ['channel_1','channel_2','channel_3','channel_4']
});
```

Get state

```bash
const state = this.appState.get();
```

### Helper functions in `global.js`

1. Put comma for numbers for example 1000000 = 1,000,000

```bash
putComma = (num) => {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
```

2. Debounce method for better performance of app

```bash
debounce = (func, delay) => {
    let debounceTimer;
    return function () {
        const context = this;
        const args = arguments;
        clearTimeout(debounceTimer);
        debounceTimer = setTimeout(() => func.apply(context, args), delay)
    }
};
```

3. Array sorting for Numbers and String

```bash
const explodeDotNotation = (obj, notation) => notation.split('.').reduce((object, index) => object[index], obj);

Array.prototype.sortByPropNum = function (propName) {
    return this.sort((a, b) => explodeDotNotation(b, propName) - explodeDotNotation(a, propName));
};

Array.prototype.sortByPropString = function (propName) {
    return this.sort((a, b) => explodeDotNotation(a, propName) > explodeDotNotation(b, propName) ? 1 : -1);
};

############ Ussage:
array.sortByPropNum('statistics.videoCount')
```

## Tested on:

* Google Chrome 76.0.3809.132
* Firefox 69.0
* Opera
* Safari IOS, Google Chrome IOS (iPhone 8 latest version)
* MS Edge
* IE 11 - not supported (but it possible - polyfill for Object.assign)

